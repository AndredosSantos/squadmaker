const createRandomSquad = (listOfDevs, devs_in_squad = 2) => {
  const squads = [];

  while (listOfDevs.length > 0) {
    const squad = [];

    for (let indice = 0; indice <= devs_in_squad; indice++) {
      if (listOfDevs.length != 0 && squad.length < devs_in_squad) {
        let dev_indice = Math.round(Math.random(0, listOfDevs.length) * 10);

        while (listOfDevs[dev_indice] === undefined) {
          dev_indice = Math.round(Math.random(0, listOfDevs.length) * 10);
        }

        dev = listOfDevs.filter((_, index) => index === dev_indice)[0];
        listOfDevs = listOfDevs.filter((_, index) => index !== dev_indice);
        squad.push(dev);
      }
    }
    squads.push(squad);
  }

  return squads;
};

const showSquads = (listOfDevs, devs_in_squad) => {
  const div = document.getElementById("squads");
  div.innerHTML = "";
  const squads = createRandomSquad(listOfDevs, devs_in_squad);
  for (let squad of squads) {
    const li = document.createElement("li");
    for (let dev of squad) {
      const h2 = document.createElement("h2");

      h2.innerText = dev;
      li.appendChild(h2);
    }
    li.classList.add("squad");
    div.appendChild(li);
  }
};

const buttonDuo = document.getElementById("button-duo");
const buttonTeam = document.getElementById("button-team");

const handleClickFunction = (devs_in_squad = 2) => {
  const textArea = document.getElementById("textarea").value;
  const listOfDevs = textArea.split("\n");
  showSquads(listOfDevs, devs_in_squad);
};

buttonDuo.addEventListener("click", () => handleClickFunction(2));
buttonTeam.addEventListener("click", () => handleClickFunction(4));
